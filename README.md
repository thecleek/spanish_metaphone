# Metahphone en español

Implementación de metaphone en español en php y python.

Compatibles entre sí.



### Php

La implementación original en php la obtuve de [aquí](http://www.calvilloweb.com/spanish_metaphone.php)


### Python

La implementación de python es compatible con la implementación en php.

Esta traducción a python es software libre (como en cerveza gratis) y puedes usarlo como mejor te parezca.
