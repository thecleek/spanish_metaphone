#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Implementación del double metaphone para español ampliamente basada en esta http://www.calvilloweb.com/spanish_metaphone.php

Pero en python :P

Esta traducción a python es software libre. Haz lo que gustes con ella.

"""

def spanish_metaphone(string):
	vocales = ['A', 'E', 'I', 'O', 'U']
	normalizeChars = {'Á': 'A', 'À': 'A', 'Â': 'A', 'Ã': 'A', 'Å': 'A', 'Ä': 'A', 'Æ': 'AE', 'Ç': 'C', 'É': 'E', 'È': 'E', 'Ê': 'E', 'Ë': 'E', 'Í': 'I', 'Ì': 'I', 'Î': 'I', 'Ï': 'I', 'Ð': 'Eth', 'Ñ': 'N', 'Ó': 'O', 'Ò': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ø': 'O', 'Ú': 'U', 'Ù': 'U', 'Û': 'U', 'Ü': 'U', 'Ý': 'Y', 'á': 'a', 'à': 'a', 'â': 'a', 'ã': 'a', 'å': 'a', 'ä': 'a', 'æ': 'ae', 'ç': 'c', 'é': 'e', 'è': 'e', 'ê': 'e', 'ë': 'e', 'í': 'i', 'ì': 'i', 'î': 'i', 'ï': 'i', 'ð': 'eth', 'ñ': 'n', 'ó': 'o', 'ò': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', 'ú': 'u', 'ù': 'u', 'û': 'u', 'ü': 'u', 'ý': 'y', 'ß': 'sz', 'þ': 'thorn', 'ÿ': 'y' ,'z': 'S', 'b': 'V'}
	
	#Inicializamos la cadena de claves metaphone
	meta_key = ''
	
	#Establecemos el máximo de la cadena
	key_length = 11
	
	#Posición actual
	current_pos = 0
	
	#la longitud de la cadena original
	string_length = len(string)
	
	#Le pegamos algunos espacios para que no se salga nunca del rango
	original_string = string + "      "
	
	#Reemplazamos algunos caracteres
	for toReplace in normalizeChars:
		original_string = original_string.replace(toReplace, normalizeChars[toReplace])
	
	original_string = original_string.upper()
	
	while len(meta_key) < key_length:
		if current_pos >= string_length:
			break
		
		current_char = original_string[current_pos]
		
		#Si empieza con una vocal, dejamos la vocal
		if (original_string[current_pos] in vocales) and (current_pos == 0):
			meta_key += current_char
			current_pos += 1
		
		#Estas consonantes tienen un sonido único o ya fueron reemplazadas
		#por unas con el mismo sonido
		elif original_string[current_pos] in ['D', 'F', 'J', 'K', 'M', 'N', 'P', 'R', 'S', 'T', 'V']:
			meta_key += current_char
			
			#Si hay una letra repetida, no la tomamos en cuenta
			if original_string[current_pos+1] == current_char:
				current_pos += 2
			current_pos += 1
		
		#Estas letras tienen varios casos dependiendo qué les siga
		else:
			if current_char == 'C':
				#Macho, charo, etc.
				if original_string[current_pos + 1] == 'H':
					meta_key += 'K'
					current_pos += 2
				#Acción, reacción
				elif original_string[current_pos + 1] == 'C':
					meta_key += 'X'
					current_pos += 2
				#César, cien
				elif original_string[current_pos:current_pos+2] == 'CE' or original_string[current_pos:current_pos+2] == 'CI':
					meta_key += 'S'
					current_pos += 2
				else:
					meta_key += 'K'
					current_pos += 1
			
			elif current_char == 'G':
				#Gente, ecología
				if original_string[current_pos:current_pos+2] == 'GE' or original_string[current_pos:current_pos+2] == 'GI':
					meta_key += 'J'
					current_pos += 2
				else:
					meta_key += 'G'
					current_pos += 1
			
			#Hache muda, tomamos la vocal que le siga.
			elif current_char == 'H':
				if original_string[current_pos + 1] in vocales:
					meta_key += original_string[current_pos + 1]
					current_pos += 2
				else:
					meta_key += 'H'
					current_pos += 1
			
			elif current_char == 'Q':
				#Queso
				if original_string[current_pos + 1] == 'U':
					current_pos += 2
				else:
					current_pos += 1
				meta_key += 'K'
			
			elif current_char == 'W':
				meta_key += 'U'
				current_pos += 2
			
			elif current_char == 'X':
				#Xochicalpán, xoloscuintle
				if current_pos == 0:
					meta_key += 'S'
					current_pos += 2
				else:
					meta_key += 'X'
					current_pos += 1
			
			else:
				current_pos += 1
		'''
		#Esto para probar :)
		print 'original_string', '\'', original_string, '\''
		print 'current_pos', current_pos
		print 'meta_key', meta_key
		'''
	meta_key = meta_key.strip()
	return meta_key


if __name__ == '__main__' :
	palabras = ['X','xplosion','escalera','scalera','mi','tu','su','te','ochooomiiiillllllll','complicado','ácaro','ácido','clown','down','col','clon','waterpolo','aquino','rebosar','rebozar','grajea','gragea','encima','enzima','alhamar','abollar','aboyar','huevo','webo','macho','xocolate','chocolate','axioma','abedul','a','gengibre','yema','wHISKY','google','xilófono','web','guerra','pingüino','si','ke','que','tu','gato','gitano','queso','paquete','cuco','perro','pero','arrebato','hola', 'zapato', 'españa', 'garrulo', 'expansión', 'membrillo', 'jamón','risa','caricia', 'llaves', 'paella','cerilla']
	#words = ['grajea', 'gragea']
	
	for s in palabras:
		smp = spanish_metaphone(s)
		print s, '->', smp


